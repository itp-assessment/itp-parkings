using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Itp.RouteManagement.ExternalServices.Test
{
    public class BestRouteFinderTest
    {
        [Fact]
        public async Task Find_ShouldReturnAsBestRoute_TheFirstRouteProposedByGoogleMapWebsite()
        {
            // Google map visual sample
            // https://www.google.be/maps/dir/Interparking+Brussels+Airport+Zaventem,+Brussel-Nationaal+Luchthaven,+1930+Zaventem/Parking+Roodebeek+(200),+1200+Woluwe-Saint-Lambert/@50.8717607,4.418146,13z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x47c3dd5831d400c1:0x93c43732d24c3705!2m2!1d4.481413!2d50.896344!1m5!1m1!1s0x47c3dc9f142bca7f:0x804b81b2ca670200!2m2!1d4.4357742!2d50.8484916!3e0

            // Expected api call
            // https://maps.googleapis.com/maps/api/directions/json?units=meters&mode=driving&alternatives=false&origin=50.896344%2C4.481413&destination=50.8484916%2C4.4357742&key=AIzaSyCo0qmKg1ugfuzDYVV63aycdGFKqMJJyqA

            var options = new Mock<IOptionsMonitor<GoogleApiOptions>>();
            var logger = new Mock<ILogger<GoogleBestRouteFinder>>();

            options.Setup(x => x.CurrentValue).Returns(new GoogleApiOptions { DirectionApiKey = "AIzaSyCo0qmKg1ugfuzDYVV63aycdGFKqMJJyqA" });

            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("https://maps.googleapis.com");
            
            var sut = new GoogleBestRouteFinder(new GoogleDirectionService(httpClient), options.Object, logger.Object);
            
            var result = await sut.Find(new Models.Location(50.896344, 4.481413), new Models.Location(50.8484916, 4.4357742));

            result.Should().BeEquivalentTo(new Models.RouteSummary
            {
                DistanceInMeter = 8290,
                PathSummary = "R22",
                DurationInSeconds = 832
            });
        }
    }
}
