﻿using Itp.RouteManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace Itp.RouteManagement.Sqlite
{
    public class RoutesDbContext : DbContext
    {
        public DbSet<Route> Routes { get; set; }
        public RoutesDbContext(DbContextOptions<RoutesDbContext> options)
        : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // https://docs.microsoft.com/en-us/ef/core/modeling/owned-entities
            modelBuilder.Entity<Route>(route =>
            {
                // TODO complete the relational model
                route.OwnsOne(x => x.Departure, d => d.OwnsOne(x => x.Location));
                route.OwnsOne(x => x.Destination, d => d.OwnsOne(x => x.Location));
                route.HasIndex(x => x.Reference).IsUnique();
            });
            //modelBuilder.Entity<Route>().HasKey("Id");
            modelBuilder.Entity<Location>(x => x.HasNoKey());
        }
    }
}
