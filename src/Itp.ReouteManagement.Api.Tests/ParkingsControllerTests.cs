using FluentAssertions;
using Itp.RouteManagement.Models;
using Itp.RouteManagement.Helper;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Itp.RouteManagement.Api.Tests
{
    public class ParkingsControllerTests : IClassFixture<StubWebApplicationFactory<Startup>>
    {
        private readonly StubWebApplicationFactory<Startup> _factory;

        public ParkingsControllerTests(StubWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task Get_ShouldReturn_Interparking_When_SearchingFor_Zaventem()
        {
            var client = _factory.CreateClient();

            var result = await client.GetAsync("/parkings?search=zaventem");

            result.StatusCode.Should().Be(HttpStatusCode.OK);
            var parkings = await result.Content.ReadAsJsonAsync<IEnumerable<Parking>>();

            // https://maps.googleapis.com/maps/api/place/textsearch/json?language=en&query=zaventem&type=parking&key=AIzaSyCo0qmKg1ugfuzDYVV63aycdGFKqMJJyqA

            var itpZaventem = parkings.FirstOrDefault(x => x.Name.Equals("Interparking Brussels Airport Zaventem"));

            itpZaventem.Should().NotBeNull();
            itpZaventem.FormattedAddress.Should().Be("Brussel-Nationaal Luchthaven, 1930 Zaventem, Belgium");
            itpZaventem.Location.Should().BeEquivalentTo(new Location(50.896344, 4.481413));
        }
    }
}
