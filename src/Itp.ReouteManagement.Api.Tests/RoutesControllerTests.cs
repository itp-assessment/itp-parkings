﻿using FluentAssertions;
using Itp.RouteManagement.Models;
using Itp.RouteManagement.Helper;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Itp.RouteManagement.Api.Controllers;
using Moq;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;

namespace Itp.RouteManagement.Api.Tests
{
    public class RoutesControllerTests : IClassFixture<StubWebApplicationFactory<Startup>>
    {
        private readonly StubWebApplicationFactory<Startup> _factory;

        public RoutesControllerTests(StubWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task Create_WithSameLocationForDepartureAndDestination_BadRequest()
        {
            var client = _factory.CreateClient();
            
            var result = await client.PostAsync("/routes", CreateRouteRequest("Fake", "same-id", "same-id"));

            result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }


        [Fact]
        public async Task Create_A_Route_WithData_Provided_By_BestRouteFinder()
        {
            var repo = new Mock<IRouteRepository>();            
            var parkingRepo = new Mock<IParkingRepository>();            
            var from = new Location(10, 12);
            var to = new Location(20, 22);
            parkingRepo.Setup(x => x.GetByReference("parking1")).Returns(FakeParking("From", from, "parking1"));
            parkingRepo.Setup(x => x.GetByReference("parking2")).Returns(FakeParking("To", to, "parking2"));

            var stubBestRoute = new Mock<IBestRouteFinder>();
            stubBestRoute.Setup(x => x.Find(from, to)).Returns(Task.FromResult(new RouteSummary
            {
                DistanceInMeter = 200,
                PathSummary = "E40 - A12"
            }));

            var client = CreateClientWithStubs(services =>
            {
                services.AddScoped(_ => parkingRepo.Object);
                services.AddScoped(_ => stubBestRoute.Object);
                services.AddScoped(_ => repo.Object);
            });

            var result = await client.PostAsync("/routes", CreateRouteRequest("To Work", "parking1", "parking2"));

            result.StatusCode.Should().Be(HttpStatusCode.Created);
            var route = await result.Content.ReadAsJsonAsync<RouteDto>();

            route.Name.Should().Be("To Work");
            route.Departure.Should().BeEquivalentTo(FakeParking("From", from, "parking1").Result);
            route.Destination.Should().BeEquivalentTo(FakeParking("To", to, "parking2").Result);
            route.DistanceInMeters.Should().Be(200);
            route.PathSummary.Should().Be("E40 - A12");
        }

        private HttpClient CreateClientWithStubs(Action<IServiceCollection> injectStubs)
            => _factory.WithWebHostBuilder(builder =>
               {
                   builder.ConfigureTestServices(injectStubs);
               }).CreateClient();

        private Task<Parking> FakeParking(string name, Location location, string id) 
            => Task.FromResult(new Parking
            {
                Name = name,
                Location = location,
                Reference = id
            });
        
        private CreateRouteRequest CreateRouteRequest(string name, string fromId, string toId)
            => new CreateRouteRequest
            {
                DepartureReference = fromId,
                DestinationReference = toId,
                Name= name
            };
    }
}
