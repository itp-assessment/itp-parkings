﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Itp.RouteManagement.Helper
{
    public static class HttpContentExtensions
    {
        public static async Task<T> ReadAsJsonAsync<T>(this HttpContent content)
        {
            var data = await content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(data);
        }
    }
    public static class HttpClientExtensions
    {
        public static Task<HttpResponseMessage> PostAsync<T>(this HttpClient client, string requestUri, T payload)
            => client.PostAsync(
                requestUri, 
                new StringContent(JsonConvert.SerializeObject(payload), Encoding.UTF8, "application/json"));
        
    }
}
