﻿using System;

namespace Itp.RouteManagement.Models
{
    public class Route
    {
        private Route()
        { }
        public Guid Reference { get; internal set; }
        public string Name { get; set; }
        public string PathSummary { get; private set; }
        public int DistanceInMeters { get; private set; }
        public int DurationInSecond { get; private set; }
        public Parking Departure { get; private set; }
        public Parking Destination { get; private set; }

        public static Route Create(string name, Parking from, Parking to, RouteSummary summary)
        {
            if (summary == null)
                throw new ArgumentNullException(nameof(summary));
            
            return new Route
            {
                Reference = Guid.NewGuid(),
                Name = !string.IsNullOrEmpty(name) ? name : throw new ArgumentNullException(nameof(name)),
                Departure = from ?? throw new ArgumentNullException(nameof(from)),
                Destination = to ?? throw new ArgumentNullException(nameof(to)),
                PathSummary = summary.PathSummary,
                DistanceInMeters = summary.DistanceInMeter
            };
        }
    }
}
