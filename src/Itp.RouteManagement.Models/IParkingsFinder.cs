﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Itp.RouteManagement.Models
{
    public interface IParkingsFinder
    {
        Task<IEnumerable<Parking>> SearchByText(string freeText);
    }
}
