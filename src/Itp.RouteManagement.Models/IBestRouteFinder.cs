﻿using System;
using System.Threading.Tasks;

namespace Itp.RouteManagement.Models
{
    public interface IBestRouteFinder
    {
        Task<RouteSummary> Find(Location from, Location to);
    }
    public interface IRouteRepository
    {
        Task<Route> GetAsync(Guid reference);
        Task SaveAsync(Route route);
    }
}
