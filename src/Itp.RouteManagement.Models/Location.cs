﻿using System.Globalization;

namespace Itp.RouteManagement.Models
{
    public class Location : ValueObject<Location>
    {
        private  Location()
        {
        }
        public Location(double latitude, double longitude)
        {
            Longitude = longitude;
            Latitude = latitude;
        }
        public double Longitude { get; }
        public double Latitude { get; }
        public override string ToString() => $"(Lat:{Latitude.ToString("G", CultureInfo.InvariantCulture)}," +
            $"Lgn:{Longitude.ToString("G", CultureInfo.InvariantCulture)})";
    }
}
