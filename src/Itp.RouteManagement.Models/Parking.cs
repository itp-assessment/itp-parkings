﻿namespace Itp.RouteManagement.Models
{
    public class Parking
    {
        public string Reference { get; set; }
        public string FormattedAddress { get; set; }
        public string Name { get; set; }
        public Location Location { get; set; }
    }
}
