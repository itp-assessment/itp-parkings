﻿using System.Threading.Tasks;

namespace Itp.RouteManagement.Models
{
    public interface IParkingRepository
    {
        Task<Parking> GetByReference(string reference);
    }
}
