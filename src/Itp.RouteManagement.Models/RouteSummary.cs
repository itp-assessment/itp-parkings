﻿namespace Itp.RouteManagement.Models
{
    public class RouteSummary
    {
        public string PathSummary { get; set; }
        public int DistanceInMeter { get; set; }
        public int DurationInSeconds { get; set; }
        public string Summary { get; set; }
    }
}
