﻿using Itp.RouteManagement.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Itp.RouteManagement.ExternalServices
{
    public class GoogleBestRouteFinder : IBestRouteFinder
    {
        private readonly ILogger<GoogleBestRouteFinder> _logger;
        private readonly string _apiKey;
        private readonly GoogleDirectionService _service;
        public GoogleBestRouteFinder(GoogleDirectionService service, IOptionsMonitor<GoogleApiOptions> optionAccessor, ILogger<GoogleBestRouteFinder> logger)
        {
            var apiKey = optionAccessor.CurrentValue.DirectionApiKey;
            _logger = logger;
            _apiKey = !string.IsNullOrEmpty(apiKey) ? apiKey : throw new ArgumentNullException(nameof(apiKey));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<RouteSummary> Find(Location from, Location to)
        {
            try
            {
                var result = await _service.Directions(from,to,false, _apiKey, "en");
                
                var bestRoute = result.Routes.FirstOrDefault();

                return new RouteSummary
                {
                    DistanceInMeter = bestRoute?.Legs.Sum(x => x.Distance.Value) ?? 0,
                    DurationInSeconds = bestRoute?.Legs.Sum(x => x.Duration.Value) ?? 0,
                    PathSummary = bestRoute?.Summary
                };
            }
            catch (Exception ex)
            {
                _logger.LogError($"Find best route from {from} to {to} has failed. {ex}");
                throw;
            }
        }
    }
}
    

