﻿using Itp.RouteManagement.Helper;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Itp.RouteManagement.ExternalServices
{
    // https://developers.google.com/places/web-service/search
    public class GooglePlaceService
    {
        private readonly HttpClient _httpClient;

        public GooglePlaceService(HttpClient client)
        {
            _httpClient = client;
        }
        public async Task<SearchPlacesresult> TextSearch(string query, string apiKey, string language)
        {
            var response = await _httpClient.GetAsync($"/maps/api/place/textsearch/json?query={query}&type=parking&key={apiKey}&language={language}");

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsJsonAsync<SearchPlacesresult>();
        }

        public async Task<PlaceDetaisResult> Details(string placeId, string apiKey, string language)
        {
            var response = await _httpClient.GetAsync($"/maps/api/place/details/json?place_id={placeId}&type=parking&key={apiKey}&language={language}");

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsJsonAsync<PlaceDetaisResult>();
        }
        public class SearchPlacesresult
        {
            public IEnumerable<Parking> Results { get; set; }
        }
        public class PlaceDetaisResult
        {
            public Parking Result { get; set; }
        }
        public class Parking
        {
            [JsonProperty("formatted_address")]
            public string FormattedAddress { get; set; }
            public string Name { get; set; }
            public Geometry Geometry { get; set; }
            [JsonProperty("place_id")]
            public string PlaceId { get; set; }
        }
        public class Geometry
        {
            public Location Location { get; set; }
        }
        public class Location
        {
            public double Lat { get; set; }
            public double Lng { get; set; }
        }
    }
}
    

