﻿namespace Itp.RouteManagement.ExternalServices
{
    public class GoogleApiOptions
    {
        public string ApiBaseUrl { get; set; }
        public string PlaceApiKey { get; set; }
        public string DirectionApiKey { get; set; }
    }
}
