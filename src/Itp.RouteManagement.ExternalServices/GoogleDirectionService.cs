﻿using Itp.RouteManagement.Helper;
using Itp.RouteManagement.Models;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace Itp.RouteManagement.ExternalServices
{
    public class GoogleDirectionService
    {
        private readonly HttpClient _httpClient;

        public GoogleDirectionService(HttpClient client)
        {
            _httpClient = client;
        }
        // https://developers.google.com/maps/documentation/directions/intro#UnitSystems
        public async Task<RoutesResult> Directions(
            Location from, 
            Location to, 
            bool proposeAlternatives, 
            string apiKey, 
            string language)
        {
            var parameters = $"alternatives={proposeAlternatives}&origin={Location(from)}&destination={Location(to)}";

            var response = await _httpClient.GetAsync(
                $"/maps/api/directions/json?units=meters&mode=driving&key={apiKey}&language={language}"
                +$"{parameters}");

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsJsonAsync<RoutesResult>();
        }
        private string Location(Location l)
            => $"{l.Latitude.ToString("G", CultureInfo.InvariantCulture)},{l.Longitude.ToString("G", CultureInfo.InvariantCulture)}";


        public class RoutesResult
        {
            public IEnumerable<Route> Routes { get; set; }
        }
        public class Route
        {
            public IEnumerable<Leg> Legs { get; set; }
            public string Summary { get; set; }
        }
        public class Leg
        {
            public TextValue Distance { get; set; }
            public TextValue Duration { get; set; }
        }
        public class TextValue
        {
            public string Text { get; set; }
            public int Value { get; set; }
        }
    }
}
