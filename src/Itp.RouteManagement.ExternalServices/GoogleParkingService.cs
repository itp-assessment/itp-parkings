﻿using Itp.RouteManagement.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Itp.RouteManagement.ExternalServices
{
    public class GoogleParkingService : IParkingsFinder, IParkingRepository
    {
        private readonly ILogger<GoogleParkingService> _logger;
        private readonly string _apiKey;
        private readonly GooglePlaceService _service;
        public GoogleParkingService(GooglePlaceService service, IOptionsMonitor<GoogleApiOptions> optionAccessor, ILogger<GoogleParkingService> logger)
        {
            var apiKey = optionAccessor.CurrentValue.PlaceApiKey;
            _logger = logger;
            _apiKey = !string.IsNullOrEmpty(apiKey) ? apiKey : throw new ArgumentNullException(nameof(apiKey));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<Parking> GetByReference(string reference)
        {
            try
            {
                var result = (await _service.Details(reference, _apiKey, "en")).Result;

                return  new Parking
                {
                    Reference = result.PlaceId,
                    FormattedAddress = result.FormattedAddress,
                    Name = result.Name,
                    Location = new Location(result.Geometry.Location.Lat, result.Geometry.Location.Lng)
                };
            }
            catch (Exception ex)
            {
                _logger.LogError($"Reading place with id {reference} has failed. {ex}");
                throw;
            }
        }

        public async Task<IEnumerable<Parking>> SearchByText(string freeText)
        {
            try
            {
                var result = await _service.TextSearch(freeText, _apiKey, "en");
                
                return result.Results.Select(x => new Parking
                {
                    Reference = x.PlaceId,
                    FormattedAddress = x.FormattedAddress,
                    Name = x.Name,
                    Location = new Location(x.Geometry.Location.Lat, x.Geometry.Location.Lng)
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Searching places with query {freeText} has failed. {ex}");
                throw;
            }
        }
    }
}
    

