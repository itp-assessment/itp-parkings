using System;
using Itp.RouteManagement.ExternalServices;
using Itp.RouteManagement.Models;
using Itp.RouteManagement.Sqlite;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Itp.RouteManagement.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<RoutesDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            services.AddControllers();
           
            services.AddHttpClient<GooglePlaceService>(
                client => client.BaseAddress = new Uri(GetGoogleOptions().ApiBaseUrl));
            services.AddHttpClient<GoogleDirectionService>(
                client => client.BaseAddress = new Uri(GetGoogleOptions().ApiBaseUrl));

            // TODO: The performance should be improved by adding caching decorators
            // We can alvo improve he reliability with retry decorators (Polly)
            // https://github.com/App-vNext/Polly/wiki/Polly-and-HttpClientFactory

            services.AddTransient<IParkingsFinder, GoogleParkingService>();
            services.AddTransient<IParkingRepository, GoogleParkingService>();
            services.AddTransient<IBestRouteFinder, GoogleBestRouteFinder>();

            services.Configure<GoogleApiOptions>(Configuration.GetSection("GoogleApi"));
        }

        private GoogleApiOptions GetGoogleOptions()
            => Configuration.GetSection("GoogleApi").Get<GoogleApiOptions>();

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
