﻿using Itp.RouteManagement.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace Itp.RouteManagement.Api.Controllers
{
    public class CreateRouteRequest
    {
        [Required]
        public string DepartureReference { get; set; }
        [Required]
        public string DestinationReference { get; set; }
       
        [Required]
        public string Name { get; set; }
    }

    public class RouteDto
    {
        public Guid Reference { get; set; }
        public string Name { get; set; }
        public string PathSummary { get; set; }
        public int DistanceInMeters { get; set; }
        public int DurationInSecond { get;  set; }
        public Parking Departure { get;  set; }
        public Parking Destination { get;  set; }

        public static RouteDto Map(Route route)
            => new RouteDto
            {
                Name = route.Name,
                Departure = route.Departure,
                Destination = route.Destination,
                DistanceInMeters = route.DistanceInMeters,
                DurationInSecond = route.DurationInSecond,
                PathSummary = route.PathSummary,
                Reference = route.Reference
            };
    }
}
