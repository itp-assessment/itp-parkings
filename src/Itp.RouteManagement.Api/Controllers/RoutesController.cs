﻿using Itp.RouteManagement.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Itp.RouteManagement.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RoutesController : ControllerBase
    {
        private readonly IBestRouteFinder _bestRouteService;
        private readonly IRouteRepository _repository;
        private readonly IParkingRepository _parkingRepository;

        public RoutesController(
            IBestRouteFinder service,
            IRouteRepository repository,
            IParkingRepository parkingRepository)
        {
            _bestRouteService = service;
            _repository = repository;
            _parkingRepository = parkingRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateRouteRequest request)
        {
            if (request.DepartureReference.Equals(request.DestinationReference, StringComparison.InvariantCultureIgnoreCase))
            {
                return new BadRequestObjectResult("Departure and destination location should be different to create a route");
            }

            var parkingDeparture = await _parkingRepository.GetByReference(request.DepartureReference);
            var parkingDestination = await _parkingRepository.GetByReference(request.DestinationReference);

            var bestRoute = await _bestRouteService.Find(parkingDeparture.Location, parkingDestination.Location);

            var route = Route.Create(request.Name, parkingDeparture, parkingDestination, bestRoute);

            await _repository.SaveAsync(route);

            return CreatedAtAction(nameof(GetByReference), new { reference = route.Reference }, RouteDto.Map(route));
        }

        [HttpGet("{reference}")]
        public async Task<RouteDto> GetByReference(Guid reference)
        {
            var route = await _repository.GetAsync(reference);
            return RouteDto.Map(route);
        }

    }
}
