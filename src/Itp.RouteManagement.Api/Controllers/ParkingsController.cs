﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Itp.RouteManagement.Models;
using Microsoft.AspNetCore.Mvc;

namespace Itp.RouteManagement.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ParkingsController : ControllerBase
    {
        private readonly IParkingsFinder _service;

        public ParkingsController(IParkingsFinder service)
        {
           _service = service;
        }

        [HttpGet]
        public Task<IEnumerable<Parking>> Get([FromQuery]SearchParkingsRequest request)
            => _service.SearchByText(request.Search);
    }

    public class SearchParkingsRequest
    {
        [Required]
        [StringLength(100, MinimumLength =5)]        
        public string Search { get; set; }
    }

}
